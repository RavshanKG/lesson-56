
    const tasks = [
        {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
        {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
        {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
        {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
        {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
        {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
        {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
    ];
    /*Task1*/
    const frontend = tasks.filter((categoryFrontEnd) => categoryFrontEnd.category === "Frontend");
    const sumOfSpentTime = frontend.reduce((totalSum, time) => {
        totalSum += time.timeSpent;
        return totalSum;}, 0); console.log('Task1. ' + ' Time spent: ' + sumOfSpentTime);

    /*Task2*/
    let bug = tasks.filter((typeBug) => typeBug.type === "bug");
    const sumOfTimeSpent2 = bug.reduce((totalSum, time) => {
        totalSum += time.timeSpent;
        return totalSum; }, 0); console.log('Task2. ' + ' Spent time: ' + sumOfTimeSpent2);

    /*Task 3*/
    const ui = tasks.filter ((task) => task.title.includes('UI')).length;
    console.log('Task3. ' + ' Quantity of UI: ' + ui);


    /*Task 4*/
    const backendAndFrontend = tasks.reduce((acc, task) => {
        acc[task.category] ? acc[task.category]++ : acc[task.category] = 1;
        return acc;
    }, {});
    console.log('Task4. ' + JSON.stringify(backendAndFrontend));


    /*Task5*/
    const moreThanFourHours = tasks.filter((task) => task.timeSpent >= 4);
    const titleAndCategory = moreThanFourHours.map((tc) => [tc.title, tc.category]);
    console.log('Task5. ' + JSON.stringify(titleAndCategory));


